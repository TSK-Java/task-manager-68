package ru.tsc.kirillov.tm.api.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;

public interface IProjectDtoRepository extends JpaRepository<ProjectDto, String> {

}
