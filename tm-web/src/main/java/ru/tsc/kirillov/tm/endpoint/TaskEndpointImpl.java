package ru.tsc.kirillov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kirillov.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.kirillov.tm.dto.model.TaskDto;
import ru.tsc.kirillov.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/task")
@WebService(endpointInterface = "ru.tsc.kirillov.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpointImpl implements ITaskEndpoint {

    @NotNull
    @Autowired
    private TaskService taskService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<TaskDto> findAll() {
        return taskService
                .findAll()
                .stream()
                .collect(Collectors.toList());
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskDto findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return taskService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return taskService.existsById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public TaskDto save(
            @NotNull
            @WebParam(name = "task")
            @RequestBody final TaskDto task
    ) {
        return taskService.save(task);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "task")
            @RequestBody final TaskDto task
    ) {
        taskService.remove(task);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "tasks")
            @RequestBody final List<TaskDto> tasks
    ) {
        taskService.remove(tasks);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        taskService.clear();
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        taskService.removeById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return taskService.count();
    }
    
}
