package ru.tsc.kirillov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.UserUpdateProfileRequest;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class UserUpdateProfileListener extends AbstractUserListener {

    @NotNull
    @Override
    public String getName() {
        return "update-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Изменение данных профиля пользователя.";
    }

    @Override
    @EventListener(condition = "@userUpdateProfileListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Изменение данных профиля пользователя]");
        System.out.println("Введите имя");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Введите фамилию");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("Введите отчество");
        @NotNull final String middleName = TerminalUtil.nextLine();
        getUserEndpoint().updateProfileUser(new UserUpdateProfileRequest(getToken(), firstName, lastName, middleName));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
