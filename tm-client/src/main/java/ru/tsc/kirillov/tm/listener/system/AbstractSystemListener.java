package ru.tsc.kirillov.tm.listener.system;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.kirillov.tm.api.service.IPropertyService;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.listener.AbstractListener;

@Getter
@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @Nullable
    public Role[] getRoles() {
        return null;
    }

}
