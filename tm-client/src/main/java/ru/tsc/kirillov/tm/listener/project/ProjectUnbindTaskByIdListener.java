package ru.tsc.kirillov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.ProjectUnbindTaskByIdRequest;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class ProjectUnbindTaskByIdListener extends AbstractProjectTaskListener {

    @NotNull
    @Override
    public String getName() {
        return "project-unbind-task-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отвязать задачу от проекта.";
    }

    @Override
    @EventListener(condition = "@projectUnbindTaskByIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Отвязка задачи от проекта]");
        System.out.println("Введите ID проекта:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("Введите ID задачи:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final ProjectUnbindTaskByIdRequest request = new ProjectUnbindTaskByIdRequest(getToken(), projectId, taskId);
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        getProjectTaskEndpoint().unbindTaskToProjectId(request);
    }

}
