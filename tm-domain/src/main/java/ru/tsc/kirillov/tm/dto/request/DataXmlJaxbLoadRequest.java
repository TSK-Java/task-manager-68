package ru.tsc.kirillov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataXmlJaxbLoadRequest extends AbstractUserRequest {

    public DataXmlJaxbLoadRequest(@Nullable final String token) {
        super(token);
    }
}
