package ru.tsc.kirillov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kirillov.tm.dto.model.TaskDto;

import java.util.List;

public interface ITaskDtoRepository extends IWbsDtoRepository<TaskDto> {

    @NotNull
    @Query("FROM TaskDto WHERE userId = :userId AND projectId = :projectId")
    List<TaskDto> findAllByProjectId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId
    );

    @Modifying
    @Transactional
    @Query("DELETE FROM TaskDto WHERE userId = :userId AND projectId = :projectId")
    void removeAllByProjectId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId
    );

    @Modifying
    @Transactional
    @Query("DELETE FROM TaskDto WHERE userId = :userId AND projectId IN (:projects)")
    void removeAllByProjectList(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projects") String[] projects
    );

}
