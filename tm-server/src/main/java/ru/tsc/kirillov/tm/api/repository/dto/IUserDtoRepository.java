package ru.tsc.kirillov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kirillov.tm.dto.model.UserDto;

public interface IUserDtoRepository extends IDtoRepository<UserDto> {

    @Nullable
    UserDto findFirstByLogin(@NotNull String login);

    @Nullable
    UserDto findFirstByEmail(@NotNull String email);

    @Nullable
    @Transactional
    UserDto deleteByLogin(@NotNull String login);

}
